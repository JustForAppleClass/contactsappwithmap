//
//  displayPersonViewController.m
//  CoreDataContacts
//
//  Created by Matthew Griffin on 7/10/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "displayPersonViewController.h"

@interface displayPersonViewController ()

@end

@implementation displayPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationRecieved:) name:@"Display" object:nil];
    UIButton* editPersonButton = [UIButton buttonWithType:UIButtonTypeSystem];
    editPersonButton.frame = CGRectMake(0, 0, 100, 20);
    
    [editPersonButton setTitle:@"edit contact" forState:UIControlStateNormal];
    
    [editPersonButton addTarget:self action:@selector(editContact:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* editPerson = [[UIBarButtonItem alloc]initWithCustomView:editPersonButton];
    
    self.navigationItem.rightBarButtonItem = editPerson;
    
    UIScrollView* scroller = [UIScrollView new];
    [scroller setTranslatesAutoresizingMaskIntoConstraints:NO];
    scroller.backgroundColor = [UIColor blackColor];
    [scroller setScrollEnabled:YES];
    [self.view addSubview:scroller];
    
    NSNumber* height = [NSNumber numberWithDouble:self.view.frame.size.height];
    NSNumber* width = [NSNumber numberWithDouble:self.view.frame.size.width/2];
    NSNumber* width2 = [NSNumber numberWithDouble:self.view.frame.size.width];
    
    NSDictionary *metrics = @{
                              @"h":height,
                              @"w":width,
                              @"w2":width2,
                              };
    NSDictionary *view = @{
                           @"scroll":scroller
                           
                           };
    
    NSArray* constrainer = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scroll(h)]" options:0 metrics:metrics views:view];
    [self.view addConstraints:constrainer];
    
    constrainer = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scroll]|" options:0 metrics:metrics views:view];
    [self.view addConstraints:constrainer];
    
    //start putting stuff on the scroller
    UILabel *namelbl = [UILabel new];
    [namelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    namelbl.text = @"Name:";
    namelbl.textColor = [UIColor whiteColor];
    namelbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:namelbl];
    
    
    UILabel* address1lbl = [UILabel new];
    [address1lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    address1lbl.text = @"Address line 1:";
    address1lbl.textColor = [UIColor whiteColor];
    address1lbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:address1lbl];
    
    UIImage* picture = [[UIImage alloc]initWithData:[self.displayPerson valueForKey:@"picture"]];
    self.showImage = [[UIImageView alloc]initWithImage:picture];
    [self.showImage setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scroller addSubview:self.showImage];
    
    //Buttons for doing things with contact info
    UIButton* mapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [mapButton setTitle:@"Map" forState:UIControlStateNormal];
    [mapButton setBackgroundColor:[UIColor orangeColor]];
    [mapButton  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [mapButton setTintColor:[UIColor whiteColor]];
    [mapButton addTarget:self action:@selector(mapNow:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:mapButton];
    
    UIButton* call = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [call setTitle:@"Call" forState:UIControlStateNormal];
    [call setBackgroundColor:[UIColor orangeColor]];
    [call  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [call setTintColor:[UIColor whiteColor]];
    [call addTarget:self action:@selector(callNow:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:call];
    
    UIButton* text = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [text setTitle:@"Text" forState:UIControlStateNormal];
    [text setBackgroundColor:[UIColor purpleColor]];
    [text  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [text setTintColor:[UIColor whiteColor]];
    [text addTarget:self action:@selector(textNow:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:text];
    
    UIButton* emailBut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [emailBut setTitle:@"Send email" forState:UIControlStateNormal];
    [emailBut setBackgroundColor:[UIColor orangeColor]];
    [emailBut  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [emailBut setTintColor:[UIColor whiteColor]];
    [emailBut addTarget:self action:@selector(emailNow:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:emailBut];
    
    UILabel* phonelbl = [UILabel new];
    [phonelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    phonelbl.text = @"Phone number:";
    
    phonelbl.textColor = [UIColor whiteColor];
    phonelbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:phonelbl];
    
    UILabel* address2lbl = [UILabel new];
    [address2lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    address2lbl.text = @"Address 2:";
    address2lbl.textColor = [UIColor whiteColor];
    address2lbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:address2lbl];
    
    UILabel* city = [UILabel new];
    [city setTranslatesAutoresizingMaskIntoConstraints:NO];
    city.text = @"City:";
    city.textColor = [UIColor whiteColor];
    city.backgroundColor = [UIColor redColor];
    [scroller addSubview:city];
    
    UILabel* state = [UILabel new];
    [state setTranslatesAutoresizingMaskIntoConstraints:NO];
    state.text = @"State:";
    state.textColor = [UIColor whiteColor];
    state.backgroundColor = [UIColor redColor];
    [scroller addSubview:state];
    
    UILabel* email = [UILabel new];
    [email setTranslatesAutoresizingMaskIntoConstraints:NO];
    email.text = @"Email:";
    email.textColor = [UIColor whiteColor];
    email.backgroundColor = [UIColor redColor];
    [scroller addSubview:email];
    
    self.name = [UILabel new];
    [self.name setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.name.backgroundColor = [UIColor whiteColor];
    self.name.text = [self.displayPerson valueForKey:@"name"];
    
    [scroller addSubview:self.name];
    self.address1= [UILabel new];
    [self.address1 setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.address1.backgroundColor = [UIColor whiteColor];
    self.address1.text = [self.displayPerson valueForKey:@"address1"];
    [scroller addSubview:self.address1];
    
    self.phone = [UILabel new];
    self.phone.text = [self.displayPerson valueForKey:@"phone_number"];
    [self.phone setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.phone.backgroundColor = [UIColor whiteColor];
    [scroller addSubview:self.phone];
    
    self.address2 = [UILabel new];
    [self.address2 setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.address2.text = [self.displayPerson valueForKey:@"address2"];
    self.address2.backgroundColor = [UIColor whiteColor];
    [scroller addSubview:self.address2];
    
    self.city = [UILabel new];
    self.city.text = [self.displayPerson valueForKey:@"city"];
    [self.city setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.city.backgroundColor = [UIColor whiteColor];
    [scroller addSubview:self.city];
    
    self.state = [UILabel new];
    self.state.text = [self.displayPerson valueForKey:@"state"];
    [self.state setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.state.backgroundColor = [UIColor whiteColor];
    [scroller addSubview:self.state];
    
    self.email = [UILabel new];
    self.email.text = [self.displayPerson valueForKey:@"email_address"];
    [self.email setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.email.backgroundColor = [UIColor whiteColor];
    [scroller addSubview:self.email];
    
    UIView* util = [UIView new];
    [util setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scroller addSubview:util];
    
    

    NSDictionary *views = @{
                            @"name":self.name,
                            @"namelbl":namelbl,
                            @"phonelbl":phonelbl,
                            @"phone":self.phone,
                            @"address1":self.address1,
                            @"address1lbl":address1lbl,
                            @"address2lbl":address2lbl,
                            @"address2":self.address2,
                            @"citylbl":city,
                            @"city":self.city,
                            @"statelbl":state,
                            @"state":self.state,
                            @"emailbl":email,
                            @"email":self.email,
                            @"util":util,
                            @"map":mapButton,
                            @"call":call,
                            @"text":text,
                            @"emailB":emailBut,
                            @"picture":self.showImage,
                            
                            };
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[picture(<=200)]-[namelbl(20)]-[phonelbl(20)]-[address1lbl(20)]-[address2lbl(20)]-[citylbl(20)]-[statelbl(20)]-[emailbl(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[picture(<=200)]-[namelbl(20)]-[call(20)]-[map(20)]-[address2lbl(20)]-[citylbl(20)]-[statelbl(20)]-[emailB(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[picture(<=200)]-[namelbl(20)]-[text(20)]-[map(20)]-[address2lbl(20)]-[citylbl(20)]-[statelbl(20)]-[emailB(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];

    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[picture]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[namelbl][name]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[phonelbl][phone][call][text(call)]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[address1lbl][address1][map]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[address2lbl][address2]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[citylbl][city]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[statelbl][state]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[emailbl][email][emailB]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[util(w2)]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[picture(<=200)]-[name(20)]-[phone(20)]-[address1(20)]-[address2(20)]-[city(20)]-[state(20)]-[email(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];

    
}

-(void)editContact:(id)sender
{
    editPersonViewController* epvc = [editPersonViewController new];
    epvc.editPerson = self.displayPerson;
    epvc.context = self.context;
    [self.navigationController pushViewController:epvc animated:YES];
    
}

-(void)NotificationRecieved:(NSNotification*)n
{
    self.displayPerson = n.object;
    self.city.text = [self.displayPerson valueForKey:@"city"];
    self.name.text = [self.displayPerson valueForKey:@"name"];
    self.phone.text = [self.displayPerson valueForKey:@"phone_number"];
    self.address1.text = [self.displayPerson valueForKey:@"address1"];
    self.address2.text = [self.displayPerson valueForKey:@"address2"];
    self.email.text = [self.displayPerson valueForKey:@"email_address"];
    self.state.text = [self.displayPerson valueForKey:@"state"];
    UIImage* tempImg = [UIImage imageWithData:[self.displayPerson valueForKey:@"picture"]];
    [self.showImage setImage:tempImg];
}

-(void)mapNow:(id)sender
{
    MapperViewController* map = [MapperViewController new];
    map.address1 = [self.displayPerson valueForKey:@"address1"];
    map.city = [self.displayPerson valueForKey:@"city"];
    map.state = [self.displayPerson valueForKey:@"state"];
    map.address2 = [self.displayPerson valueForKey:@"address2"];
    map.name = [self.displayPerson valueForKey:@"name"];
    [self.navigationController pushViewController:map animated:YES];
}

-(void)callNow:(id)sender
{
        NSString *cleanedString = [[[self.displayPerson valueForKey:@"phone_number"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
}

-(void)textNow:(id)sender
{
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        NSString *cleanedString = [[[self.displayPerson valueForKey:@"phone_number"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
    NSLog(@"%@", cleanedString);
    
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = @"SMS message here";
            controller.recipients = [NSArray arrayWithObjects:cleanedString, nil];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:^{
    
            }];
        }
    
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:controller completion:^{
        
    }];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:controller completion:^{
        
    }];
}


-(void)emailNow:(id)sender
{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"My Subject"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        [controller setToRecipients:@[[self.displayPerson valueForKey:@"email_address"]]];
        [self presentViewController:controller animated:YES completion:^{
    
        }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
