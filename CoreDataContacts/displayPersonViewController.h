//
//  displayPersonViewController.h
//  CoreDataContacts
//
//  Created by Michelle Griffin on 7/10/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "appDelegate.h"
#import "editPersonViewController.h"
#import "MapperViewController.h"

@interface displayPersonViewController : UIViewController<MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property(strong, nonatomic)NSManagedObject* displayPerson;
@property(strong, nonatomic)NSManagedObjectContext* context;
@property(strong,nonatomic)UILabel* name;
@property(strong,nonatomic)UILabel* address1;
@property(strong,nonatomic)UILabel* address2;
@property(strong,nonatomic)UILabel* city;
@property(strong,nonatomic)UILabel* state;
@property(strong,nonatomic)UILabel* email;
@property(strong,nonatomic)UILabel* phone;
@property(strong,nonatomic)UIImageView* showImage;




@end
