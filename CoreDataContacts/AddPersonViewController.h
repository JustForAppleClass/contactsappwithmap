//
//  AddPersonViewController.h
//  CoreDataContacts
//
//  Created by Matthew Griffin on 7/8/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddPersonViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(strong,nonatomic)UITextField* name;
@property(strong,nonatomic)UITextField* address1;
@property(strong,nonatomic)UITextField* address2;
@property(strong,nonatomic)UITextField* city;
@property(strong,nonatomic)UITextField* state;
@property(strong,nonatomic)UITextField* email;
@property(strong,nonatomic)UITextField* phone;
@property(strong,nonatomic)UIButton* addPic;
@property(strong, nonatomic)UIImageView* showImage;
@property(strong, nonatomic)NSManagedObjectContext * context;

@end
