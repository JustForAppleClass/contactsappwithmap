//
//  editPersonViewController.m
//  CoreDataContacts
//
//  Created by Michelle Griffin on 7/10/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "editPersonViewController.h"

@interface editPersonViewController ()

@end

@implementation editPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSNumber* height = [NSNumber numberWithDouble:self.view.frame.size.height];
    NSNumber* width = [NSNumber numberWithDouble:self.view.frame.size.width/2];
    NSNumber* width2 = [NSNumber numberWithDouble:self.view.frame.size.width];
    
    
    UIButton* updateContact = [UIButton buttonWithType:UIButtonTypeSystem];
    updateContact.frame = CGRectMake(0, 0, 120, 20);
    
    [updateContact setTitle:@"update contact" forState:UIControlStateNormal];
    
    [updateContact addTarget:self action:@selector(updateContact:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* update = [[UIBarButtonItem alloc]initWithCustomView:updateContact];
    
    self.navigationItem.rightBarButtonItem = update;
    UIScrollView* scroller = [UIScrollView new];
    [scroller setTranslatesAutoresizingMaskIntoConstraints:NO];
    scroller.backgroundColor = [UIColor blackColor];
    [scroller setScrollEnabled:YES];
    [self.view addSubview:scroller];
    
    NSDictionary *metrics = @{
                              @"h":height,
                              @"w":width,
                              @"w2":width2,
                              };
    NSDictionary *view = @{
                           @"scroll":scroller
                           
                           };
    
    NSArray* constrainer = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scroll(h)]" options:0 metrics:metrics views:view];
    [self.view addConstraints:constrainer];
    
    constrainer = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scroll]|" options:0 metrics:metrics views:view];
    [self.view addConstraints:constrainer];
    
    //start putting stuff on the scroller
    UILabel *namelbl = [UILabel new];
    [namelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    namelbl.text = @"Name:";
    namelbl.textColor = [UIColor whiteColor];
    namelbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:namelbl];
    
    UIButton* ChangePicture = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [ChangePicture setTitle:@"Change Picture" forState:UIControlStateNormal];
    [ChangePicture setBackgroundColor:[UIColor orangeColor]];
    [ChangePicture  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [ChangePicture setTintColor:[UIColor whiteColor]];
    [ChangePicture addTarget:self action:@selector(changePicture:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:ChangePicture];
    
    self.saveImage = [[UIImage alloc]initWithData:[self.editPerson valueForKey:@"picture"]];
    self.showImage = [[UIImageView alloc]initWithImage:self.saveImage];
    [self.showImage setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scroller addSubview:self.showImage];
    
    
    UILabel* address1lbl = [UILabel new];
    [address1lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    address1lbl.text = @"Address line 1:";
    address1lbl.textColor = [UIColor whiteColor];
    address1lbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:address1lbl];
    
    UILabel* phonelbl = [UILabel new];
    [phonelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    phonelbl.text = @"Phone number:";
    phonelbl.textColor = [UIColor whiteColor];
    phonelbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:phonelbl];
    
    UILabel* address2lbl = [UILabel new];
    [address2lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    address2lbl.text = @"Address 2:";
    address2lbl.textColor = [UIColor whiteColor];
    address2lbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:address2lbl];
    
    UILabel* city = [UILabel new];
    [city setTranslatesAutoresizingMaskIntoConstraints:NO];
    city.text = @"City:";
    city.textColor = [UIColor whiteColor];
    city.backgroundColor = [UIColor redColor];
    [scroller addSubview:city];
    
    UILabel* state = [UILabel new];
    [state setTranslatesAutoresizingMaskIntoConstraints:NO];
    state.text = @"State:";
    state.textColor = [UIColor whiteColor];
    state.backgroundColor = [UIColor redColor];
    [scroller addSubview:state];
    
    UILabel* email = [UILabel new];
    [email setTranslatesAutoresizingMaskIntoConstraints:NO];
    email.text = @"Email:";
    email.textColor = [UIColor whiteColor];
    email.backgroundColor = [UIColor redColor];
    [scroller addSubview:email];
    
    self.name = [UITextField new];
    self.name.text = [self.editPerson valueForKey:@"name"];
    [self.name setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.name.backgroundColor = [UIColor whiteColor];
    self.name.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.name];
    self.address1= [UITextField new];
    self.address1.text = [self.editPerson valueForKey:@"address1"];
    [self.address1 setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.address1.backgroundColor = [UIColor whiteColor];
    self.address1.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.address1];
    
    self.phone = [UITextField new];
    self.phone.text = [self.editPerson valueForKey:@"phone_number"];
    [self.phone setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.phone.backgroundColor = [UIColor whiteColor];
    self.phone.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.phone];
    
    self.address2 = [UITextField new];
    self.address2.text = [self.editPerson valueForKey:@"address2"];
    [self.address2 setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.address2.backgroundColor = [UIColor whiteColor];
    self.address2.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.address2];
    
    self.city = [UITextField new];
    self.city.text = [self.editPerson valueForKey:@"city"];
    [self.city setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.city.backgroundColor = [UIColor whiteColor];
    self.city.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.city];
    
    self.state = [UITextField new];
    self.state.text = [self.editPerson valueForKey:@"state"];
    [self.state setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.state.backgroundColor = [UIColor whiteColor];
    self.state.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.state];
    
    self.email = [UITextField new];
    self.email.text = [self.editPerson valueForKey:@"email_address"];
    [self.email setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.email.backgroundColor = [UIColor whiteColor];
    self.email.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.email];
    
    UIView* util = [UIView new];
    [util setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [scroller addSubview:util];
    
    
    NSDictionary *views = @{
                            @"name":self.name,
                            @"namelbl":namelbl,
                            @"phonelbl":phonelbl,
                            @"phone":self.phone,
                            @"address1":self.address1,
                            @"address1lbl":address1lbl,
                            @"address2lbl":address2lbl,
                            @"address2":self.address2,
                            @"citylbl":city,
                            @"city":self.city,
                            @"statelbl":state,
                            @"state":self.state,
                            @"emailbl":email,
                            @"email":self.email,
                            @"util":util,
                            @"button":ChangePicture,
                            @"picture":self.showImage,
                            
                            };
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[button]-[picture(<=200)]-[namelbl(20)]-[phonelbl(20)]-[address1lbl(20)]-[address2lbl(20)]-[citylbl(20)]-[statelbl(20)]-[emailbl(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[picture]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[namelbl][name]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[phonelbl][phone]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[address1lbl][address1]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[address2lbl][address2]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[citylbl][city]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[statelbl][state]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[emailbl][email]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[util(w2)]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[button]-[picture(<=200)]-[name(20)]-[phone(20)]-[address1(20)]-[address2(20)]-[city(20)]-[state(20)]-[email(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateContact:(id)sender
{
    NSError *error;
    [self.editPerson setValue:self.name.text forKey:@"name"];
    [self.editPerson setValue:self.address1.text forKey:@"address1"];
    [self.editPerson setValue:self.address2.text forKey:@"address2"];
    [self.editPerson setValue:self.phone.text forKey:@"phone_number"];
    [self.editPerson setValue:self.city.text forKey:@"city"];
    [self.editPerson setValue:self.state.text forKey:@"state"];
    [self.editPerson setValue:self.email.text forKey:@"email_address"];
    NSData* imageData = UIImageJPEGRepresentation(self.saveImage, 0.8);
    [self.editPerson setValue:imageData forKey:@"picture"];
    
    if (![self.context save:&error]) {
                   NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Display" object:self.editPerson];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)changePicture:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self dismissViewControllerAnimated:picker completion:^{
        
    }];
    
    self.saveImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [self.showImage setImage:self.saveImage];
    
    
    
    
   
}

-(UIView*)GimmeFinishButton {
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton* btn = [ UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(self.view.frame.size.width - 100, 0, 100, 50);
    [view addSubview:btn];
    
    return view;
}

-(void)done:(id)sender
{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
