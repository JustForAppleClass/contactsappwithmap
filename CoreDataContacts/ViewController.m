//
//  ViewController.m
//  CoreDataContacts
//
//  Created by Matthew Griffin on 7/8/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.starting = [NSNumber numberWithInt:0];
    self.view.backgroundColor = [UIColor purpleColor];
    self.navigationController.navigationBar.topItem.title = @"Contacts";
    appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UIButton* editPersonButton = [UIButton buttonWithType:UIButtonTypeSystem];
    editPersonButton.frame = CGRectMake(0, 0, 100, 20);
    
    [editPersonButton setTitle:@"add contact" forState:UIControlStateNormal];
    
    [editPersonButton addTarget:self action:@selector(addPerson:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* addPerson = [[UIBarButtonItem alloc]initWithCustomView:editPersonButton];
    
    self.navigationItem.rightBarButtonItem = addPerson;
    
    //Uncomment to add a person on upon loading
    /*NSManagedObjectContext *context = [appDel managedObjectContext];
    NSManagedObject *person = [NSEntityDescription
                               insertNewObjectForEntityForName:@"Person"
                               inManagedObjectContext:context];
    [person setValue:@"Doug Parsing" forKey:@"name"];
    [person setValue:@"3848 Harrison Blvd" forKey:@"address1"];
    [person setValue:@"Ogden" forKey:@"city"];
    [person setValue:@"Utah" forKey:@"state"];
    [person setValue:@"adamhellewell@weber.edu" forKey:@"email_address"];
    [person setValue:@"8017250192" forKey:@"phone_number"];
    
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }*/
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNumber* width = [NSNumber numberWithDouble:self.view.frame.size.width];
    NSNumber* height = [NSNumber numberWithDouble:self.view.frame.size.height];
    
    
    self.tableview = [[UITableView alloc]init];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview setTranslatesAutoresizingMaskIntoConstraints: NO];
    NSDictionary *metrics = @{@"width":width,
                              @"height":height,
                              @"start":self.starting};
    NSDictionary *views = @{
                            @"table":self.tableview,
                            
                            };
    
    [self.view addSubview:self.tableview];
    
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-start-[table(height)]" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[table]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    //self.starting = [NSNumber numberWithInt:self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height];
    [super viewWillDisappear:animated];
    [self.tableview removeFromSuperview];
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSManagedObjectContext *context = [appDel managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if(error)
    {
        NSLog(@"Error, data not retrieved");
        return 0;
    }
    return fetchedObjects.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"tcell"];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tcell"];
    }
    
    NSManagedObjectContext *context = [appDel managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* current = fetchedObjects[indexPath.row];
    cell.textLabel.text = [current valueForKey:@"name"];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    displayPersonViewController* display = [displayPersonViewController new];
    NSManagedObjectContext *context = [appDel managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* current = fetchedObjects[indexPath.row];
    display.displayPerson = current;
    display.context = context;
    [self.navigationController pushViewController:display animated:YES];
}

-(void)addPerson:(id)sender
{
    AddPersonViewController* addingPerson = [AddPersonViewController new];
    addingPerson.context = [appDel managedObjectContext];
    [self.navigationController pushViewController:addingPerson animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
