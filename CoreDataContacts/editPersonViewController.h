//
//  editPersonViewController.h
//  CoreDataContacts
//
//  Created by Michelle Griffin on 7/10/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface editPersonViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(strong, nonatomic)NSManagedObject* editPerson;
@property(strong, nonatomic)NSManagedObjectContext * context;
@property(strong,nonatomic)UITextField* name;
@property(strong,nonatomic)UITextField* address1;
@property(strong,nonatomic)UITextField* address2;
@property(strong,nonatomic)UITextField* city;
@property(strong,nonatomic)UITextField* state;
@property(strong,nonatomic)UITextField* email;
@property(strong,nonatomic)UITextField* phone;
@property(strong,nonatomic)UIImage* saveImage;
@property(strong, nonatomic)UIImageView* showImage;

@end
