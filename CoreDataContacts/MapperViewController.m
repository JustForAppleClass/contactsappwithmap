//
//  MapperViewController.m
//  CoreDataContacts
//
//  Created by Michelle Griffin on 7/12/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "MapperViewController.h"

@interface MapperViewController ()

@end

@implementation MapperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        MKMapView* map = [[MKMapView alloc]init];
    [map setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:map];
    UIButton* MapApp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [MapApp setTitle:@"Go To Maps" forState:UIControlStateNormal];
    [MapApp setBackgroundColor:[UIColor orangeColor]];
    [MapApp  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [MapApp setTintColor:[UIColor whiteColor]];
    [MapApp addTarget:self action:@selector(GoToMapApp:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:MapApp];
    
    NSDictionary *metrics = @{
                              
                              };
    NSDictionary *views = @{
                            @"map":map,
                            @"MAB":MapApp,
                            
                            };
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[map][MAB]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[map]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[MAB]|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
        map.delegate = self;
        map.showsUserLocation = YES;
    
    NSString* address = [NSString stringWithFormat:@"%@ %@, %@ %@", self.address1, self.address2, self.city, self.state];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
            for (CLPlacemark* aPlacemark in placemarks)
            {
                
                MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                [annotation setCoordinate:aPlacemark.location.coordinate];
                [annotation setTitle:self.name]; //You can set the subtitle too
                [annotation setSubtitle:address];
                [map addAnnotation:annotation];
                
                MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
                region.center.latitude = aPlacemark.location.coordinate.latitude ;
                region.center.longitude = aPlacemark.location.coordinate.longitude;
                region.span.longitudeDelta = 0.009f;
                region.span.latitudeDelta = 0.009f;
                [map setRegion:region animated:YES];
            }
        }];
    
        
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    
}

-(void)GoToMapApp:(id)sender
{
    
    NSString* address = [NSString stringWithFormat:@"%@ %@, %@ %@", self.address1, self.address2, self.city, self.state];
    NSString* cleanedAddres = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",[address stringByReplacingOccurrencesOfString:@" " withString:@"+"]];
    NSURL *url = [NSURL URLWithString:cleanedAddres];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
