//
//  ViewController.h
//  CoreDataContacts
//
//  Created by Matthew Griffin on 7/8/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AddPersonViewController.h"
#import "displayPersonViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    AppDelegate* appDel;
    NSManagedObject* Nmo;
}

@property(strong, nonatomic) UITableView* tableview;
@property(strong, nonatomic) NSNumber* starting;


@end

