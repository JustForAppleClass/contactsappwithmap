//
//  MapperViewController.h
//  CoreDataContacts
//
//  Created by Michelle Griffin on 7/12/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface MapperViewController : UIViewController<MKMapViewDelegate>


@property (nonatomic, retain) CLLocationManager* locationManager;
@property(nonatomic, strong)NSString* address1;
@property(nonatomic, strong)NSString* address2;
@property(nonatomic, strong)NSString* city;
@property(nonatomic,  strong)NSString* state;
@property(nonatomic, strong)NSString* name;


@end
