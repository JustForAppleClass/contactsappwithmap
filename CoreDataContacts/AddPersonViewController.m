//
//  AddPersonViewController.m
//  CoreDataContacts
//
//  Created by Matthew Griffin on 7/8/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "AddPersonViewController.h"

@interface AddPersonViewController ()

@end

@implementation AddPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton* editPersonButton = [UIButton buttonWithType:UIButtonTypeSystem];
    editPersonButton.frame = CGRectMake(0, 0, 100, 20);
    
    [editPersonButton setTitle:@"add contact" forState:UIControlStateNormal];
    
    [editPersonButton addTarget:self action:@selector(addPerson:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* addPerson = [[UIBarButtonItem alloc]initWithCustomView:editPersonButton];
    
    self.navigationItem.rightBarButtonItem = addPerson;
    NSNumber* height = [NSNumber numberWithDouble:self.view.frame.size.height];
    NSNumber* width = [NSNumber numberWithDouble:self.view.frame.size.width/2];
    NSNumber* width2 = [NSNumber numberWithDouble:self.view.frame.size.width];
    
    UIScrollView* scroller = [UIScrollView new];
    [scroller setTranslatesAutoresizingMaskIntoConstraints:NO];
    scroller.backgroundColor = [UIColor blackColor];
    [scroller setScrollEnabled:YES];
        [self.view addSubview:scroller];
    
    NSDictionary *metrics = @{
                              @"h":height,
                              @"w":width,
                              @"w2":width2,
                              };
    NSDictionary *view = @{
                           @"scroll":scroller
                           
                              };
    
    NSArray* constrainer = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scroll(h)]" options:0 metrics:metrics views:view];
    [self.view addConstraints:constrainer];
    
    constrainer = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scroll]|" options:0 metrics:metrics views:view];
    [self.view addConstraints:constrainer];
    
    //start putting stuff on the scroller
    UILabel *namelbl = [UILabel new];
    [namelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    namelbl.text = @"Name:";
    namelbl.textColor = [UIColor whiteColor];
    namelbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:namelbl];
    
    self.addPic = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.addPic setTitle:@"Add picture" forState:UIControlStateNormal];
    [self.addPic setBackgroundColor:[UIColor orangeColor]];
    [self.addPic  setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.addPic setTintColor:[UIColor whiteColor]];
    [self.addPic addTarget:self action:@selector(addPic:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:self.addPic];
    
    self.showImage = [[UIImageView alloc]init];
    [self.showImage setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scroller addSubview:self.showImage];
    
    UILabel* address1lbl = [UILabel new];
    [address1lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    address1lbl.text = @"Address line 1:";
    address1lbl.textColor = [UIColor whiteColor];
    address1lbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:address1lbl];
    
    UILabel* phonelbl = [UILabel new];
    [phonelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    phonelbl.text = @"Phone number:";
    phonelbl.textColor = [UIColor whiteColor];
    phonelbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:phonelbl];
    
    UILabel* address2lbl = [UILabel new];
    [address2lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    address2lbl.text = @"Address 2:";
    address2lbl.textColor = [UIColor whiteColor];
    address2lbl.backgroundColor = [UIColor redColor];
    [scroller addSubview:address2lbl];
    
    UILabel* city = [UILabel new];
    [city setTranslatesAutoresizingMaskIntoConstraints:NO];
    city.text = @"City:";
    city.textColor = [UIColor whiteColor];
    city.backgroundColor = [UIColor redColor];
    [scroller addSubview:city];
    
    UILabel* state = [UILabel new];
    [state setTranslatesAutoresizingMaskIntoConstraints:NO];
    state.text = @"State:";
    state.textColor = [UIColor whiteColor];
    state.backgroundColor = [UIColor redColor];
    [scroller addSubview:state];
    
    UILabel* email = [UILabel new];
    [email setTranslatesAutoresizingMaskIntoConstraints:NO];
    email.text = @"Email:";
    email.textColor = [UIColor whiteColor];
    email.backgroundColor = [UIColor redColor];
    [scroller addSubview:email];
    
    self.name = [UITextField new];
    [self.name setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.name.backgroundColor = [UIColor whiteColor];
    self.name.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.name];
     self.address1= [UITextField new];
    [self.address1 setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.address1.backgroundColor = [UIColor whiteColor];
    self.address1.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.address1];
    
    self.phone = [UITextField new];
    [self.phone setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.phone.backgroundColor = [UIColor whiteColor];
    self.phone.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.phone];
    
    self.address2 = [UITextField new];
    [self.address2 setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.address2.backgroundColor = [UIColor whiteColor];
    self.address2.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.address2];
    
    self.city = [UITextField new];
    [self.city setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.city.backgroundColor = [UIColor whiteColor];
    self.city.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.city];
    
    self.state = [UITextField new];
    [self.state setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.state.backgroundColor = [UIColor whiteColor];
    self.state.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.state];
    
    self.email = [UITextField new];
    [self.email setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.email.backgroundColor = [UIColor whiteColor];
    self.email.inputAccessoryView = [self GimmeFinishButton];
    [scroller addSubview:self.email];
    
    UIView* util = [UIView new];
    [util setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [scroller addSubview:util];
    
    
    NSDictionary *views = @{
                            @"name":self.name,
                            @"namelbl":namelbl,
                            @"phonelbl":phonelbl,
                            @"phone":self.phone,
                            @"address1":self.address1,
                            @"address1lbl":address1lbl,
                            @"address2lbl":address2lbl,
                            @"address2":self.address2,
                            @"citylbl":city,
                            @"city":self.city,
                            @"statelbl":state,
                            @"state":self.state,
                            @"emailbl":email,
                            @"email":self.email,
                            @"util":util,
                            @"addPic":self.addPic,
                            @"picture":self.showImage,
                            
                            };
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[addPic]-[picture(<=200)]-[namelbl(20)]-[phonelbl(20)]-[address1lbl(20)]-[address2lbl(20)]-[citylbl(20)]-[statelbl(20)]-[emailbl(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[addPic]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[picture]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[namelbl][name]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[phonelbl][phone]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[address1lbl][address1]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[address2lbl][address2]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[citylbl][city]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[statelbl][state]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[emailbl][email]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[util(w2)]|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[addPic]-[picture(<=200)]-[name(20)]-[phone(20)]-[address1(20)]-[address2(20)]-[city(20)]-[state(20)]-[email(20)]-[util(500)]-|" options:0 metrics:metrics views:views];
    [scroller addConstraints:constraints];
}

-(void)addPic:(id)sender
{
    [self.addPic setTitle:@"Change picture" forState:UIControlStateNormal];
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

-(UIView*)GimmeFinishButton {
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton* btn = [ UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(self.view.frame.size.width - 100, 0, 100, 50);
    [view addSubview:btn];
    
    return view;
}

-(void)done:(id)sender
{
    [self.view endEditing:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self dismissViewControllerAnimated:picker completion:^{
        
    }];
    
    
    [self.showImage setImage:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addPerson:(id)sender
{
    NSError *error;
    NSManagedObject* person = [NSEntityDescription
                               insertNewObjectForEntityForName:@"Person"
                               inManagedObjectContext:self.context];
    [person setValue:self.name.text forKey:@"name"];
    [person setValue:self.address1.text forKey:@"address1"];
    [person setValue:self.address2.text forKey:@"address2"];
    [person setValue:self.phone.text forKey:@"phone_number"];
    [person setValue:self.city.text forKey:@"city"];
    [person setValue:self.state.text forKey:@"state"];
    [person setValue:self.email.text forKey:@"email_address"];
    NSData* imageData = UIImageJPEGRepresentation(self.showImage.image, 0.8);
    [person setValue:imageData forKey:@"picture"];
    
    if (![self.context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation


@end
